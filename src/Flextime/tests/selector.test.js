import slice from '../FlextimeSlice';
import _ from 'lodash';
import moment from 'moment';
import SettingsUpdate from '../../UserSettings/SettingsUpdate';

const today = moment('2019-02-06');
const state = {
    flextime: {
        today,
        calander: {
        }
    }
}

const dayAsPath = day => [day.year(), day.month(), day.date()]

// Months start at zero after parsing, so Feb (2) becomes 1.
_.set(state.flextime.calander, [2019, 1, 4],
    { day: moment(today).add(1, 'days'), in: '10:11', out: '15:21' });

_.set(state.flextime.calander, [2019, 1, 5],
     { day: moment(today).add(1, 'days'), in: '10:12', out: '15:22' });

_.set(state.flextime.calander, [2019, 1, 6],
     { day: moment(today).add(2, 'days'), in: '10:13', out: '15:23' });

_.set(state.flextime.calander, [2019, 1, 7],
     { day: moment(today).add(3, 'days'), in: '10:14', out: '15:24' });

it ('Select today from the state', () => {
    const result = slice.selectors.getToday(state);
    
    expect(result.in).toBe('10:13');
    expect(result.out).toBe('15:23');
    expect(result.day).toBe(today);
})

it ('Select this week from the state', () => {
    const result = slice.selectors.getThisWeek(state);
    const monday = moment(today).startOf('isoWeek');

    expect(result.length).toBe(7);

    for(let i = 0; i < 7; i++){
        const expected = moment(monday).add(i, 'days');
        expect(result[i].day).toEqual(expected);
    }
})

it('Select the number of days completed this week', () => {
    const result = slice.selectors.getDaysCompletedThisWeek(state);

    expect(result).toBe(3);
});

it('Select the number of hours worked this week', () => {
    const today = moment().startOf('isoWeek').add(3, 'days');
    const calander = {}
    const state = { flextime: { today, calander } }

    for(let i = 0; i < 3; i++){
        const day = moment(today).subtract(i, 'days');
        _.set(calander, [day.year(), day.month(), day.date()], { in: "10:00", out: "11:00" });
    }

    const result = slice.selectors.getHoursWorkedThisWeek(state);

    expect(result).toBe(3);
});

describe('Select the amount of flex accrued this week', () => {
    function createState(dayOne, dayTwo) {
        const monday = moment().startOf('isoWeek');
        const today = moment(monday).add(3, 'days');
        const state =  { 
            flextime: { 
                today,
                calander: {}
            },
            userSettings: {
                default: new SettingsUpdate(42.5, 5),
                updates: []
            }
        }

        _.set(state.flextime.calander, dayAsPath(moment(monday)), dayOne);
        _.set(state.flextime.calander, dayAsPath(moment(monday).add(1, 'days')), dayTwo);

        return state;
    }

    it('when positive flex has been accrued', () => {
        const state = createState(
            { in: "00:00", out: "08:30" },
            { in: "00:00", out: "10:00" });

        expect(slice.selectors.getFlexAcruedThisWeek(state)).toBe(1.5);
    });

    it('when negative flex has been accrued', () => {
        const state = createState(
            { in: "00:00", out: "08:30" },
            { in: "00:00", out: "08:00" });

        //console.log("State: ", state);
        expect(slice.selectors.getFlexAcruedThisWeek(state)).toBe(-0.5);
    });

    it('when zero flex has been accrued', () => {
        const state = createState(
            { in: "00:00", out: "08:30" },
            { in: "00:00", out: "08:30" });
        console.log("INPUT: ", state);
        expect(slice.selectors.getFlexAcruedThisWeek(state)).toBe(0);
    });
})

it('selects the loading status', () => {
    const state = { flextime: { isLoading: true } }

    expect(slice.selectors.getIsLoaded(state)).toBe(false);
})