import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import flextimeSlice from '../FlextimeSlice.js';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';

function DisplayToday(props){
    if(props.hasToday) {
    return <div>
        <Typography>{props.date}</Typography>
        <Typography>In: {props.in}</Typography>
        <Typography>Out: {props.out}</Typography>
    </div>;
    } else {
        return <Typography>Loading</Typography>;
    }
}

DisplayToday.propTypes = {
    date: PropTypes.string.isRequired,
    in: PropTypes.string,
    out: PropTypes.string
}

function mapStateToProps() {
    return state => {
        state = state[flextimeSlice.slice];
        const now = moment();
        const today = selectDay(state, now);
        return {
            date: now.format('ddd MMM YYYY'),
            hasToday: today !== undefined,
            in: today && today.in,
            out: today && today.out
        };
    };
}

export default connect(
    mapStateToProps()
)(DisplayToday)

