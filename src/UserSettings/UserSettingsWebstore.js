import * as webstore from '../store/webstore.js';

const ROOT_PATH = 'userdata';

const getSettingsPath = userId => ROOT_PATH + `/${userId}/settings`;

export function watchUserSettings(userId, callback){
    return webstore.WatchPath(getSettingsPath(userId), callback);
}