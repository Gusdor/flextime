import slice from '../UserSettingsSlice';
import _ from 'lodash';

it ('Updates the state correctly', () => {
    // Arrange
    const hours = 40;
    const updates = { "2019-03-07": { requiredWeeklyHours: hours } };
    const def = { requiredWeeklyHours: hours };
    const expectedState = {
        default : def,
        updates
    }

    // Act
    const state = slice.reducer(
        { default: expectedState.default }, 
        slice.actions.userSettingsUpdated({ updates }));

    // Assert
    expect(state).toEqual(expectedState);
})