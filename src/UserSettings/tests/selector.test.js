import slice from '../UserSettingsSlice';
import SettingsUpdate from '../SettingsUpdate';
import moment from 'moment';

describe('selects the daily target', () => {
    it('when only default settings exist', () => {
        const state = {
            userSettings: {
                default: new SettingsUpdate(42.5, 5)
            }
        }
        
        const selector = slice.selectors.getDailyTarget(state);
        const result = selector(moment());
        
        expect(result).toBe(8.5);
    });
})

it ('selects settings updates with valid dates', () => {
    const state = {
        userSettings: {
            default: new SettingsUpdate(42.5, 5),
            updates: {
                'invalid': {},
                '2019-03-08': new SettingsUpdate(11, 22),
                '2019-03-07': new SettingsUpdate(33, 44)
            }
        }
    }

    const result = slice.selectors.getSettingUpdates(state);

    expect(result).toBeTruthy();
    expect(result).toEqual({
        '2019-03-07': new SettingsUpdate(33, 44),
        '2019-03-08': new SettingsUpdate(11, 22)
    });
})

describe('selects the settings for a specific day', () => {
    const state = {
        userSettings: {
            default: new SettingsUpdate(42.5, 5),
            updates: {
                'invalid': {},
                '2019-03-05': new SettingsUpdate(11, 22),
                '2019-03-06': new SettingsUpdate(33, 44),
                '2019-03-08': new SettingsUpdate(55, 66)
            }
        }
    }

    it('when day is date of latest update', () => {
        const day = moment('2019-03-08');
        const settings = slice.selectors.getSettingsForDay(day)(state);

        expect(settings).toBe(state.userSettings.updates['2019-03-08']);
    });

    it('when the day is the day after the latest update', () => {
        const day = moment('2019-03-09');
        const settings = slice.selectors.getSettingsForDay(day)(state);

        expect(settings).toBe(state.userSettings.updates['2019-03-08']);
    });

    it('when the day occurs before the latest update', () => {
        const day = moment('2019-03-07');
        const settings = slice.selectors.getSettingsForDay(day)(state);

        expect(settings).toBe(state.userSettings.updates['2019-03-06']);
    });
})