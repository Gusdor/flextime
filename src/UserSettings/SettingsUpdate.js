export default class SettingsUpdate {
    constructor(requiredWeeklyHours, workDayCount){
        this.requiredWeeklyHours = requiredWeeklyHours;
        this.workDays = workDayCount;
    }
}