import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';

import Counter from './components/Counter.js';
import CounterSlice from '../CounterSlice.js';

function Incrementer(props) {
    return <div>
        <Counter count={props.count}/>
        <Button onClick={() => props.increment(1)}>Increment</Button>
        <Button onClick={() => props.increment(-1)}>Decrement</Button>
    </div>
    }

 
 function mapDispatchToProps(dispatch, props) {
    return { 
        increment: (value) => dispatch(CounterSlice.actions.increment(value)) 
    };
}
    
function mapStateToProps(state, props) {
    return { ...props, count: state[CounterSlice.slice].count };
}

export default connect(mapStateToProps, mapDispatchToProps)(Incrementer);