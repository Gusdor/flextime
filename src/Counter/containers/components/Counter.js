import React from 'react';
import Typography from '@material-ui/core/Typography';

export default function Counter(props) {
    return <Typography> Current count is {props.count}</Typography>
}